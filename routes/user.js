const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

router.post("/register", (req, res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/details", auth.verify, (req, res) => {
		const userData = auth.decode(req.headers.authorization);

	userControllers.getProfile({userId: req.body.id}).then(resultFromController => res.send(resultFromController));
});


router.put("/:userId/client", auth.verify, (req, res) => {

	const data = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	
	userControllers.custUser(data).then(resultFromController => res.send(resultFromController));
});

router.put("/:userId/admin", auth.verify, (req, res) => {

	const data = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	
	userControllers.adminUser(data).then(resultFromController => res.send(resultFromController));
});


router.post("/purchase", auth.verify, (req, res) => {
	
	userControllers.purchaseOrder(req.body).then(resultFromController => res.send(resultFromController));
})







module.exports = router;