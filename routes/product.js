const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth.js");




router.post("/new", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productControllers.addProduct(data).then(resultFromController => res.send(resultFromController));

});




router.get("/available", (req, res) => {
	productControllers.getAllActive().then(resultFromController => res.send(resultFromController));
});


router.get("/:productId", (req, res) =>{

	productControllers.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

router.put("/:productId/update", (req, res) => {

	const data = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productControllers.updateProduct(req.params, req.body, data).then(resultFromController => res.send(resultFromController));
})




router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	
	productControllers.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId/avail", auth.verify, (req, res) => {

	const data = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	
	productControllers.availProduct(data).then(resultFromController => res.send(resultFromController));
});


module.exports = router;
