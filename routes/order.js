const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers.js")

const auth = require ("../auth.js")


router.post("/createOrder", auth.verify, (req, res) => {
	const data = {
		user: auth.decode(req.headers.authorization).id,
		order: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	orderControllers.createOrder(data).then(resultFromController => res.send (resultFromController));
})



module.exports = router;
