const Order = require("../models/Order.js");
const User = require("../models/User.js");
const Product = require("../models/Product.js")
const bcrypt = require("bcrypt")
const auth = require("../auth.js")


module.exports.createOrder = async (data) => {
	if(data.isAdmin == false){
		if(data.user == null) {
			return false;
		} else {
			const product = await Product.findById(data.order.products.productId);
			if(product.isActive){
				let newOrder = new Order({
					userId: data.user,
					products: {
						productId : data.order.products.productId,
						quantity : data.order.products.quantity
					},
					totalAmount: product.price * data.order.products.quantity
				})
				await newOrder.save();
				return true;
			} else {
				return false
			}
		}
	} else {
		return false
	}
}
