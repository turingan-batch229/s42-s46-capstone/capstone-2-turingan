const User = require("../models/User.js");
const Product = require("../models/Product.js")
const Order = require("../models/Order.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		address : reqBody.address,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		
		if (error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.loginUser = (reqBody) => {	
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
}

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		console.log(result);

		result.password = "";

		return result;
	})
}

module.exports.custUser = (data) => {

if(data.isAdmin === true){

		let updateActiveField = {
			isAdmin : false
		};

		return User.findByIdAndUpdate(data.reqParams.userId, updateActiveField).then((product, error) => {

			if (error) {

				return false;

			} else {

				return true;

			}

		});
	} else {
		return false
	}
};

module.exports.adminUser = (data) => {

if(data.isAdmin === true){

		let updateActiveField = {
			isAdmin : true
		};

		return User.findByIdAndUpdate(data.reqParams.userId, updateActiveField).then((product, error) => {

			if (error) {

				return false;

			} else {

				return true;

			}

		});
	} else {
		return false
	}
};



module.exports.placeOrder = async (data) =>{

	let isUserUpdated = await User.findById(data.userId).then(user => {

		
		user.orders.push({product: data.productId});

		
		return user.save().then((user,error) => {
		
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});

	
	let isProductUpdated = await Product.findById(data.productId).then(product =>{

		
		product.orders.push({userId: data.userId});

		
		return product.save().then((product, error) => {
			
			if(error){
				return false
			
			}else{
				return true
			};
		});
	});

	if(isUserUpdated && isCourseUpdated){
		
		return true;
	}else{
		
		return false;
		}
	}
