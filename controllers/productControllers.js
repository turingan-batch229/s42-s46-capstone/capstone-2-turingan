const Product = require("../models/Product.js");
const auth = require("../auth.js");

module.exports.addProduct = async (data) => {

	if (data.isAdmin) {		
		let newProduct = await new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	} else {
		return false;
	};	
};





module.exports.getAllActive = () => {

	return Product.find({isActive : true}).then(result => {
		return result
	});
};


module.exports.getProduct = (reqParams) => {
	console.log(reqParams);
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


module.exports.updateProduct = (reqParams, reqBody, data) => {

if(data.isAdmin === true){
	
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		
		if(error){
			return false;

	 
		}else{
			return true;
		}
	});
	} else {
		return false;
	}
}


module.exports.archiveProduct = (data) => {

if(data.isAdmin === true){

		let updateActiveField = {
			isActive : false
		};

		return Product.findByIdAndUpdate(data.reqParams.productId, updateActiveField).then((product, error) => {

			if (error) {

				return false;

			} else {

				return true;

			}

		});
	} else {
		return false;
	}
};

module.exports.availProduct = (data) => {

if(data.isAdmin === true){

		let updateActiveField = {
			isActive : true
		};

		return Product.findByIdAndUpdate(data.reqParams.productId, updateActiveField).then((product, error) => {

			if (error) {

				return false;

			} else {

				return true;

			}

		});
	} else {
		return false
	}
};