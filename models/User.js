const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required!"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required!"]
	},
	address: {
		type: String,
		required: [true, "Address is required!"]
	},
	email: {
		type: String,
		required: [true, "Email is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required!"]
	}
	/*orders: [{
		productId: {
			type: String,
			required: [true, "Product ID is required."]
		},
		totalQty: {
			type: Number,
			default: [true, "Order List"]
		},
		totalPrice: {
			type: Number,
			default: [true, "Total"]
		},
		orderedOn: {
			type: Date,
			default: new Date()
		},
		status: {
			type: String,
			default: "Paid"
		}
	}]*/

	/*orders: [{
		productId:{
			type: String,
			required: [true, "Produc ID is required."]
		},
		products:[{
		productName:{
			type: String,
			required: [true, "Product name"]
		},
		totalQty: {
			type: Number,
			default: [true, "Order list"]
		}
	}],
		totalPrice: {
			type: Number,
			default: [true, "Total"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		status: {
			type: String,
			default: "Paid"
		}
	}]*/
})

module.exports = mongoose.model("User", userSchema);